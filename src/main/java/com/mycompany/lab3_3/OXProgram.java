/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3_3;

/**
 *
 * @author informatics
 */
class OXProgram {
//Refactor

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer)) {
            return true;
        }
        if (checkCol(table, currentPlayer)) {
            return true;
        }
        if (checkDiagonal(table, currentPlayer)) {
            return true;
        }
        if (checkDiagonalRight(table, currentPlayer)) {
            return true;
        }

        return false;
    }

    static boolean checkDraw(String[][] table, String currentPlayer) {
        if (isDraw(table, currentPlayer)) {
            return true;
        }
        return false;
    }

    
     private static boolean isDraw(String[][] table, String currentPlayer) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j].equals("-")) {
                    return false;
                }
            }
        }
        return true;
    }
     
    private static boolean checkDiagonalRight(String[][] table, String currentPlayer) {
        if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkDiagonal(String[][] table, String currentPlayer) {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col < 3; col++) {
            if (checkCol(table, currentPlayer, col)) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row < 3; row++) {
            if (checkRow(table, currentPlayer, row)) {
                return true;
            }

        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    }

    private static boolean checkDiagonal(String[][] table, String currentPlayer, int col) {
        return table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer;
    }

    private static boolean checkDiagonalRight(String[][] table, String currentPlayer, int col) {
        return table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer;
    }
}
